/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

import tk.jpkrause.digitalk.protocol.layer.datalink.DataLinkLayer;
import tk.jpkrause.digitalk.protocol.layer.datalink.DataLinkLayerImpl;
import tk.jpkrause.digitalk.protocol.layer.physical.AndroidAudioInterface;
import tk.jpkrause.digitalk.protocol.layer.physical.AudioInterface;
import tk.jpkrause.digitalk.protocol.layer.physical.AudioPhysicalLayer;
import tk.jpkrause.digitalk.protocol.layer.physical.PhysicalLayer;
import tk.jpkrause.digitalk.protocol.layer.physical.PhysicalLayerParams;
import tk.jpkrause.digitalk.protocol.layer.transport.Client;
import tk.jpkrause.digitalk.protocol.layer.transport.Server;
import tk.jpkrause.digitalk.protocol.layer.transport.TimeoutHandlerImpl;
import tk.jpkrause.digitalk.protocol.layer.transport.TransportMessage;
import tk.jpkrause.digitalk.protocol.layer.transport.TransportPacketFactory;

public class MainActivity extends AppCompatActivity {

	private ListView messageList;
	private ArrayAdapter<Message> messageListAdapter;
	private DataLinkLayer dataLinkLayer;
	private PhysicalLayer physicalLayer;
	private Client client;
	private Server server;
	private CharSequence recipientId;
	private byte senderId;
	private TextView myId;
	private PhysicalLayerParams physicalLayerParams;
	private EditText messageField;
	private Button sendButton;

	private static class Message {
		private String sender, recipient, message;
		boolean sent;

		private Message(){}

		public Message(String message) {
			this.message = message;
		}

		public static Message fromTransportMessage(TransportMessage transportMessage) {
			Message message = new Message();
			message.sender = Character.toString((char)transportMessage.getSenderId());
			message.recipient = Character.toString((char)transportMessage.getRecipientId());
			message.message = new String(transportMessage.getData());
			return message;
		}

		public void setSent(boolean sent) {
			this.sent = sent;
		}

		public String getMessage() {
			return message;
		}

		@Override
		public String toString() {
			if(!TextUtils.isEmpty(sender) && !TextUtils.isEmpty(recipient)) {
				return String.format("%s %s -> %s: %s", sent ? "-" : "+", sender, recipient, message);
			} else {
				return message;
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViews();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.settings:
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		physicalLayerParams = SettingsManager.getInstance().getPhysicalLayerParams(this);
		senderId = SettingsManager.getInstance().getSourceId(MainActivity.this);
		myId.setText(getString(R.string.my_id, (char)senderId));
		if(ContextCompat.checkSelfPermission(this,
				Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED){
			initAudio();
		} else {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.RECORD_AUDIO}, 0);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(requestCode == 0 && permissions[0].equals(Manifest.permission.RECORD_AUDIO)
				&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			initAudio();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(physicalLayer != null) {
			physicalLayer.shutdown();
		}
	}

	private void initAudio() {
		AudioInterface audioInterface = new AndroidAudioInterface(physicalLayerParams);
		physicalLayer = new AudioPhysicalLayer(physicalLayerParams, audioInterface);
		physicalLayer.start();
		dataLinkLayer = new DataLinkLayerImpl(physicalLayer);
		client = new Client.Builder()
				.setDataLinkLayer(dataLinkLayer)
				.setListener(new Client.Listener() {
					@Override
					public void onError(Exception e) {
						e.printStackTrace();
						addMessage(new Message(e.getMessage()));
						onSendCompleted();
					}

					@Override
					public void onTimeout(Exception e) {
						e.printStackTrace();
						addMessage(new Message(e.getMessage()));
						onSendCompleted();
					}

					@Override
					public void onFinished() {
						addMessage(new Message(getString(R.string.message_received, recipientId)));
						onSendCompleted();
					}
				})
				.setMaxRetries(5)
				.setTimeout(2000)
				.setMessageFactory(new TransportPacketFactory())
				.setTimeoutHandler(new TimeoutHandlerImpl()).build();
		server = new Server.Builder()
				.setDataLinkLayer(dataLinkLayer)
				.setListener(new Server.Listener() {
					@Override
					public void onError(Exception e) {
						e.printStackTrace();
						addMessage(new Message(e.getMessage()));
					}

					@Override
					public void onTimeout(Exception e) {
						e.printStackTrace();
						addMessage(new Message(e.getMessage()));
					}

					@Override
					public void onFinished(TransportMessage message) {
						addMessage(Message.fromTransportMessage(message));
					}
				})
				.setMaxRetries(5)
				.setMessageFactory(new TransportPacketFactory())
				.setTimeout(2000)
				.setTimeoutHandler(new TimeoutHandlerImpl())
				.build();
		server.setSourceId(senderId);
	}

	private void initViews() {
		myId = (TextView) findViewById(R.id.my_id);
		messageField = (EditText) findViewById(R.id.message_text);
		final EditText recipientField = (EditText) findViewById(R.id.message_id);
		recipientField.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				recipientId = s;
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		sendButton = (Button) findViewById(R.id.send_button);
		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendMessage();
			}
		});
		messageList = (ListView) findViewById(R.id.message_list);
		messageListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new LinkedList<Message>());
		messageListAdapter.setNotifyOnChange(true);
		messageList.setAdapter(messageListAdapter);
		messageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String message = ((Message)parent.getItemAtPosition(position)).getMessage();
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					ClipData clip = ClipData.newPlainText(getString(R.string.app_name), message);
					clipboard.setPrimaryClip(clip);
				} else {
					android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					clipboard.setText(message);
				}
				Toast.makeText(MainActivity.this, getString(R.string.copied), Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void sendMessage() {
		String message = messageField.getText().toString();
		if(!TextUtils.isEmpty(message) && recipientId != null && recipientId.length() > 0) {
			TransportMessage transportMessage = new TransportMessage();
			transportMessage.setSenderId(senderId);
			transportMessage.setRecipientId((byte)recipientId.charAt(0));
			transportMessage.setData(message.getBytes());
			client.transmit(transportMessage);
			Message m = Message.fromTransportMessage(transportMessage);
			m.setSent(true);
			addMessage(m);
			messageField.setText("");
			sendButton.setEnabled(false);
		}
	}

	private void onSendCompleted() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				sendButton.setEnabled(true);
			}
		});
	}

	private void addMessage(final Message message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(message != null && !TextUtils.isEmpty(message.getMessage())) {
					messageListAdapter.add(message);
				}
			}
		});
	}
}
