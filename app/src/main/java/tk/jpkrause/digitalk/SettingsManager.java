/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Random;

import tk.jpkrause.digitalk.protocol.layer.physical.PhysicalLayerParams;

public class SettingsManager {

	private static final String
			SHARED_PREF_NAME = "settings",
			SOURCE_ID_PREF = "sourceId",
			PARAMS_BAUD_PREF = "paramsBaud",
			PARAMS_FREQ_ZERO = "paramsFreqZero",
			PARAMS_FREQ_ONE = "paramsFreqOne",
			PARAMS_POWER = "paramsPower";
	private final Random random = new Random();


	private static SettingsManager ourInstance = new SettingsManager();

	public static SettingsManager getInstance() {
		return ourInstance;
	}

	private SettingsManager() {}

	public byte getSourceId(Context context) {
		if(!getPrefs(context).contains(SOURCE_ID_PREF)) {
			byte id = (byte)random.nextInt();
			setSourceId(context, id);
			return id;
		} else {
			return (byte) getPrefs(context).getInt(SOURCE_ID_PREF, 0);
		}
	}

	public void setSourceId(Context context, byte id) {
		getPrefs(context).edit().putInt(SOURCE_ID_PREF, id & 0xFF).apply();
	}

	public PhysicalLayerParams getPhysicalLayerParams(Context context) {
		SharedPreferences prefs = getPrefs(context);
		if(prefs.contains(PARAMS_BAUD_PREF)
				&& prefs.contains(PARAMS_FREQ_ONE)
				&& prefs.contains(PARAMS_FREQ_ZERO)
				&& prefs.contains(PARAMS_POWER)) {
			return PhysicalLayerParams.customParams(
					prefs.getInt(PARAMS_BAUD_PREF, 0),
					prefs.getFloat(PARAMS_FREQ_ZERO, 0),
					prefs.getFloat(PARAMS_FREQ_ONE, 0),
					prefs.getInt(PARAMS_POWER, 0)
			);
		} else {
			return PhysicalLayerParams.defaultParams();
		}
	}

	public void setPhysicalLayerParams(Context context, PhysicalLayerParams physicalLayerParams) {
		getPrefs(context).edit()
				.putInt(PARAMS_BAUD_PREF, physicalLayerParams.getBaud())
				.putFloat(PARAMS_FREQ_ZERO, (float)physicalLayerParams.getFrequencyZero())
				.putFloat(PARAMS_FREQ_ONE, (float)physicalLayerParams.getFrequencyOne())
				.putInt(PARAMS_POWER, physicalLayerParams.getMinimumFrequencyPower())
				.apply();
	}

	private SharedPreferences getPrefs(Context context) {
		return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
	}
}
