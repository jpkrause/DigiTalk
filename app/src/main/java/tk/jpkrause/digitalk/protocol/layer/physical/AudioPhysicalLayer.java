/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.physical;

import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.pitch.GeneralizedGoertzel;
import be.tarsos.dsp.pitch.Goertzel;

public class AudioPhysicalLayer implements PhysicalLayer {

	private static final double TWO_PI = Math.PI * 2;
	private static final short AMPLITUDE = Short.MAX_VALUE;
	// bit masks in big-endian order
	private static final byte[] MASKS = {(byte) 0b10000000, 0b01000000, 0b00100000, 0b00010000,
			0b00001000, 0b00000100, 0b00000010, 0b00000001};
	private ReentrantLock receivingLock = new ReentrantLock();
	private Condition notReceiving = receivingLock.newCondition();

	private class TransmissionTask implements Runnable, AudioInterface.Listener {
		private static final int QUEUE_CAPACITY = 50;
		private Listener listener;
		private AudioInterface audioInterface;
		private double incrementOne, incrementZero;
		private ExecutorService executorService;
		private volatile BlockingQueue<Byte[]> queue;
		private short[] buffer, silence;
		private PhysicalLayerParams params;
		private boolean isTransmitting, run;
		private Random random;

		TransmissionTask(PhysicalLayerParams params, AudioInterface audioInterface) {
			this.queue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
			this.audioInterface = audioInterface;
			audioInterface.setListener(this);
			this.params = params;
			incrementOne = TWO_PI * params.getFrequencyOne() / params.getSampleRate();
			incrementZero = TWO_PI * params.getFrequencyZero() / params.getSampleRate();
			executorService = Executors.newSingleThreadExecutor();
			buffer = new short[params.getSamplesPerBit() * params.getBitsPerFrame()];
			silence = Arrays.copyOf(buffer, buffer.length);
			random = new Random();
		}

		void transmit(Byte[] data) {
			try {
				this.queue.put(data);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			double angle = 0; //in radians
			while(run) {
				// Carrier-Sensing
				do {
					if (isReceiving() && !isTransmitting) {
						receivingLock.lock();
						try {
							notReceiving.await();
						} catch (InterruptedException ignored) {
							continue;
						} finally {
							receivingLock.unlock();
						}
					}
					try {
						long sleepTime = (params.getCarrierSenseWaitTimeMillis()/2);
						sleepTime += (random.nextLong() % sleepTime);
						Thread.sleep(sleepTime);
					} catch (InterruptedException ignored){}
				} while (isReceiving());
				Byte[] data;
				data = queue.poll();
				if(data == null) {
					continue;
				}
				ShortBuffer shortBuffer = ShortBuffer.allocate(buffer.length * data.length);
				isTransmitting = true;

				for (Byte b : data) {
					if(b == null) {
						shortBuffer.put(silence);
						continue;
					}
					int numOnes = 0;
					//start bit
					int i = 0;
					for (int j = 0; j < params.getSamplesPerBit(); j++, i++) {
						buffer[i] = (short) (Math.sin(angle) * AMPLITUDE);
						angle = (angle + incrementZero) % TWO_PI;
					}

					for (byte mask : MASKS) {
						double increment;
						if ((b & mask) == mask) {
							numOnes++;
							increment = incrementOne;
						} else {
							increment = incrementZero;
						}
						for (int j = 0; j < params.getSamplesPerBit(); j++, i++) {
							buffer[i] = (short) (Math.sin(angle) * AMPLITUDE);
							angle = (angle + increment) % TWO_PI;
						}
					}

					// parity bit
					if (params.getNumParityBits() == 1) {
						double parityIncrement = numOnes % 2 == 0 ? incrementZero : incrementOne;
						for (int j = 0; j < params.getSamplesPerBit(); j++, i++) {
							buffer[i] = (short) (Math.sin(angle) * AMPLITUDE);
							angle = (angle + parityIncrement) % TWO_PI;
						}
					}

					//stop bit
					for (int j = 0; j < params.getSamplesPerBit(); j++, i++) {
						buffer[i] = (short) (Math.sin(angle) * AMPLITUDE);
						angle = (angle + incrementOne) % TWO_PI;
					}
					shortBuffer.put(buffer);
				}
				audioInterface.write(shortBuffer.array());
			}
		}

		@Override
		public void onTransmissionComplete() {
			isTransmitting = false;
			if(listener != null) {
				listener.onTransmitComplete();
			}
		}

		void setListener(Listener listener) {
			this.listener = listener;
		}

		void start() {
			run = true;
			executorService.execute(this);
		}

		void stop() {
			run = false;
			executorService.shutdownNow();
		}
	}

	private class ReceivingTask implements Goertzel.FrequenciesDetectedHandler {
		private Listener listener;
		private PhysicalLayerParams params;
		private byte[] data;
		private int dataIndex = 0;
		private ScheduledExecutorService timer;
		private ScheduledFuture timeoutFuture;
		private volatile boolean isReceiving;
		private AudioProcessor audioProcessor;
		private AudioInterface audioInterface;
		private Runnable timeoutRunnable = new Runnable() {
			@Override
			public void run() {
				if(isReceiving) {
					dataIndex = 0;
					isReceiving = false;
					receivingLock.lock();
					try {
						notReceiving.signalAll();
					} finally {
						receivingLock.unlock();
					}
					listener.onReceiveComplete();
				}
			}
		};

		ReceivingTask(final PhysicalLayerParams params, final AudioInterface audioInterface) {
			this.params = params;
			this.data = new byte[params.getBitsPerFrame()];
			this.audioInterface = audioInterface;

			timer = Executors.newScheduledThreadPool(1);

			final AudioProcessor goertzel = new GeneralizedGoertzel(
					params.getSampleRate(),
					params.getSamplesPerBit(),
					new double[]{params.getFrequencyZero(), params.getFrequencyOne()},
					this);

			audioProcessor = new AudioProcessor() {
				@Override
				public boolean process(AudioEvent audioEvent) {
					AudioEvent parcel = new AudioEvent(audioInterface.getAudioFormat());
					parcel.setOverlap(audioEvent.getOverlap());
					for(int j = 0; j + params.getSamplesPerBit() <= audioEvent.getBufferSize(); j+= params.getSamplesPerBit()) {
						parcel.setFloatBuffer(
								Arrays.copyOfRange(audioEvent.getFloatBuffer(), j, j + params.getSamplesPerBit() - 1));
						goertzel.process(parcel);
					}
					return true;
				}

				@Override
				public void processingFinished() {
					goertzel.processingFinished();
				}
			};
		}

		@Override
		public void handleDetectedFrequencies(double[] frequencies, double[] powers, double[] doubles2, double[] doubles3) {
			boolean bitRead = true;
			if(frequencies.length == 1 && powers[0] > params.getMinimumFrequencyPower()) {
				boolean isOne = frequencies[0] == params.getFrequencyOne();
				addBit(isOne ? (byte)0x01 : (byte)0x00);
			} else if(frequencies.length == 2) {
				boolean isOne;
				if(powers[0] > powers[1] && powers[0] > params.getMinimumFrequencyPower()) {
					isOne = frequencies[0] == params.getFrequencyOne();
					addBit(isOne ? (byte)0x01 : (byte)0x00);
				} else if(powers[1] > params.getMinimumFrequencyPower()) {
					isOne = frequencies[1] == params.getFrequencyOne();
					addBit(isOne ? (byte)0x01 : (byte)0x00);
				} else {
					bitRead = false;
				}
			} else {
				bitRead = false;
			}
			if(!bitRead) {
				boolean isPowerLow = true;
				for(double power : powers) {
					if(power > params.getMinimumFrequencyPower() / 2) {
						isPowerLow = false;
					}
				}
				if(isPowerLow) {
					startReceiveTimeout();
				}
			}
		}

		private synchronized void startReceiveTimeout() {
			if(isReceiving && timeoutFuture == null) {
				timeoutFuture = timer.schedule(timeoutRunnable,
						params.getCarrierSenseWaitTimeMillis(),
						TimeUnit.MILLISECONDS);
			}
		}

		private synchronized void stopReceiveTimeout() {
			if(timeoutFuture != null) {
				timeoutFuture.cancel(true);
				timeoutFuture = null;
			}
		}

		private void addBit(byte bit) {
			stopReceiveTimeout();
			isReceiving = true;
			data[dataIndex] = bit;
			if(dataIndex == data.length-1) {
				if(data[params.getStartBitIndex()] == 0 && data[params.getStopBitIndex()] == 1) {
					byte byteData = 0x0;
					int parity = data[params.getParityBitIndex()];
					for(int i = params.getFirstDataBitIndex(); i <= params.getLastDataBitIndex(); i++) {
						if(data[i] == 1) {
							parity++;
							byteData = (byte)(byteData | MASKS[i-1]);
						}
					}
					if(params.getNumParityBits() > 0) {
						if(parity % 2 == 0) {
							if(listener != null) {
								listener.onReceive(byteData);
							}
							dataIndex = 0;
						} else {
							//shifts bits down
							System.arraycopy(data, 1, data, 0, params.getStopBitIndex());
							data[data.length-1] = 0;
						}
					} else {
						if(listener != null) {
							listener.onReceive(byteData);
						}
						dataIndex = 0;
					}
				} else {
					//shifts bits down
					System.arraycopy(data, 1, data, 0, params.getStopBitIndex());
					data[data.length-1] = 0;
				}
			} else {
				dataIndex++;
			}
		}

		void setListener(Listener listener) {
			this.listener = listener;
		}

		void start() {
			audioInterface.addAudioProcessor(audioProcessor);
		}

		void stop() {
			audioInterface.removeAudioProcessor(audioProcessor);
			timer.shutdownNow();
		}
	}

	private TransmissionTask transmissionTask;
	private ReceivingTask receivingTask;
	private AudioInterface audioInterface;

	public AudioPhysicalLayer(PhysicalLayerParams params, AudioInterface audioInterface) {
		transmissionTask = new TransmissionTask(params, audioInterface);
		receivingTask = new ReceivingTask(params, audioInterface);
		this.audioInterface = audioInterface;
	}

	@Override
	public void start() {
		audioInterface.start();
		receivingTask.start();
		transmissionTask.start();
	}

	@Override
	public void shutdown() {
		transmissionTask.stop();
		receivingTask.stop();
		audioInterface.stop();
	}

	@Override
	public void transmit(Byte[] data) {
		transmissionTask.transmit(data);
	}

	@Override
	public boolean isReceiving() {
		return receivingTask.isReceiving;
	}

	@Override
	public boolean isTransmitting() {
		return transmissionTask.isTransmitting;
	}

	@Override
	public void setListener(Listener listener) {
		transmissionTask.setListener(listener);
		receivingTask.setListener(listener);
	}
}
