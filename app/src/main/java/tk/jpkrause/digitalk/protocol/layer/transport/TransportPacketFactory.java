/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.transport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TransportPacketFactory {

	private byte sequenceNumber;
	private Random random;

	public TransportPacketFactory(){
		random = new Random();
	}

	public void initSequenceNumber() {
		byte[] rand = new byte[1];
		random.nextBytes(rand);
		sequenceNumber = rand[0];
	}

	public TransportPacket summon(byte sourceId, byte destId) {
		byte sequenceNumber = getSequenceNumber();
		return new TransportPacket(sourceId,
				destId,
				sequenceNumber,
				sequenceNumber,
				TransportPacket.Type.SUMMON,
				null);
	}

	public TransportPacket acknowledge(byte sourceId, byte destId, byte acknowledgmentNumber) {
		return new TransportPacket(sourceId,
				destId,
				getSequenceNumber(),
				acknowledgmentNumber,
				TransportPacket.Type.ACKNOWLEDGE,
				null);
	}

	public TransportPacket awaitingOrders(byte sourceId, byte destId, byte acknowledgmentNumber) {
		byte sequenceNumber = getSequenceNumber();
		return new TransportPacket(sourceId,
				destId,
				sequenceNumber,
				acknowledgmentNumber,
				TransportPacket.Type.AWAITING_ORDERS,
				null);
	}

	public List<TransportPacket> data(byte sourceId, byte destId, byte[] data, byte acknowledgmentNumber) {
		int numPackets = (int)Math.ceil((double)data.length / TransportPacket.MAX_DATA_BYTES);
		List<TransportPacket> packets = new ArrayList<>(numPackets);
		for(int i = 0; i < numPackets; i++) {
			byte sequenceNumber = getSequenceNumber();
			int start = i*TransportPacket.MAX_DATA_BYTES;
			int end = Math.min(start + TransportPacket.MAX_DATA_BYTES, data.length);
			packets.add(new TransportPacket(sourceId,
					destId,
					sequenceNumber,
					(byte)(acknowledgmentNumber + i),
					TransportPacket.Type.DATA,
					Arrays.copyOfRange(data, start, end)));
		}
		return packets;
	}

	public TransportPacket finished(byte sourceId, byte destId, byte acknowledgmentNumber) {
		byte sequenceNumber = getSequenceNumber();
		return new TransportPacket(sourceId,
				destId,
				sequenceNumber,
				acknowledgmentNumber,
				TransportPacket.Type.FINISHED,
				null);
	}

	private byte getSequenceNumber() {
		byte val = sequenceNumber;
		sequenceNumber++;
		return val;
	}
}
