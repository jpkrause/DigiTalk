/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.transport;

import android.util.Log;

import java.util.Iterator;

import tk.jpkrause.digitalk.protocol.layer.datalink.DataLinkLayer;

public class Client implements TimeoutHandler.Callback, DataLinkLayer.Listener {

	public interface Listener {
		void onError(Exception e);
		void onTimeout(Exception e);
		void onFinished();
	}

	private enum State {
		IDLE,
		AWAITING_ACCEPT,
		SENDING_MESSAGE,
		FINISHED
	}

	private volatile State currentState, nextState;
	private Listener listener;
	private TransportPacketFactory transportPacketFactory;
	private TransportPacket currentTransportPacket;
	private Iterator<TransportPacket> dataPackets;
	private TimeoutHandler timeoutHandler;
	private DataLinkLayer dataLinkLayer;
	private long timeout;
	private int numRetries, maxRetries;
	private byte[] data;

	public static class Builder {

		Client client;

		public Builder() {
			client = new Client();
		}

		public Builder setListener(Listener listener) {
			client.listener = listener;
			return this;
		}

		public Builder setDataLinkLayer(DataLinkLayer dataLinkLayer) {
			client.dataLinkLayer = dataLinkLayer;
			return this;
		}

		public Builder setMessageFactory(TransportPacketFactory transportPacketFactory) {
			client.transportPacketFactory = transportPacketFactory;
			return this;
		}

		public Builder setTimeout(long timeout) {
			client.timeout = timeout;
			return this;
		}

		public Builder setTimeoutHandler(TimeoutHandler timeoutHandler) {
			client.timeoutHandler = timeoutHandler;
			return this;
		}

		public Builder setMaxRetries(int maxRetries) {
			client.maxRetries = maxRetries;
			return this;
		}

		public Client build() {
			client.reset();
			client.dataLinkLayer.addListener(client);
			return client;
		}
	}

	private void reset() {
		transportPacketFactory.initSequenceNumber();
		stopTimeout();
		setCurrentState(State.IDLE);
		setNextState(null);
		currentTransportPacket = null;
		dataPackets = null;
	}

	public void transmit(TransportMessage message) {
		this.data = message.getData();
		nextState = State.AWAITING_ACCEPT;
		sendSummon(message.getSenderId(), message.getRecipientId());
	}

	private void sendSummon(byte sourceId, byte destinationId) {
		currentTransportPacket = transportPacketFactory.summon(sourceId, destinationId);
		sendNextPacket();
	}

	private void sendData(byte sourceId, byte destinationId, byte acknowledgmentNumber) {
		dataPackets = transportPacketFactory.data(sourceId, destinationId, data, acknowledgmentNumber).iterator();
		currentTransportPacket = dataPackets.next();
		sendNextPacket();
	}

	private void sendFinished(byte sourceId, byte destinationId, byte acknowledgmentNumber) {
		currentTransportPacket = transportPacketFactory.finished(sourceId, destinationId, acknowledgmentNumber);
		sendNextPacket();
	}


	private void sendAcknowledged(byte destinationId, byte sourceId, byte acknowledgementNumber) {
		currentTransportPacket = transportPacketFactory.acknowledge(sourceId, destinationId, acknowledgementNumber);
		sendNextPacket();
	}

	private void startTimeout() {
		if(shouldTimeout()) {
			timeoutHandler.start(timeout, this);
		}
	}

	private void stopTimeout() {
		timeoutHandler.stop();
	}

	private void resendCurrentMessage() {
		numRetries++;
		sendCurrentMessage();
	}

	private void sendNextPacket() {
		numRetries = 0;
		sendCurrentMessage();
	}

	private void sendCurrentMessage() {
		if(currentTransportPacket != null) {
			Log.d(Client.class.getSimpleName(), "SENDING: " + currentTransportPacket.toString());
			dataLinkLayer.transmit(currentTransportPacket.compile());
		} else {
			//TODO
			Log.e(Client.class.getSimpleName(), "Error on send");
			listener.onError(new Exception());
		}
	}

	@Override
	public void onTransmitComplete() {
		if(nextState != null) {
			setCurrentState(nextState);
			startTimeout();
		}
	}

	@Override
	public void onReceiveStart() {
		timeoutHandler.pause();
	}

	@Override
	public void onReceiveError(Exception e) {
		Log.e(Client.class.getSimpleName(), "Error receiving");
		timeoutHandler.resume();
	}

	@Override
	public void onReceive(byte[] data) {
		TransportPacket packet = TransportPacket.fromBytes(data);
		if(packet != null && validateMessage(packet)) {
			Log.d(Client.class.getSimpleName(), "RECEIVED: " + packet.toString());
			if(currentTransportPacket == null
					|| packet.getAcknowledgmentNumber() == currentTransportPacket.getSequenceNumber()) {
				if(!onReceive(packet)) {
					sendCurrentMessage();
				}
			} else {
				sendCurrentMessage();
			}
		} else {
			timeoutHandler.resume();
		}
	}

	private boolean onReceive(TransportPacket transportPacket) {
		switch (currentState) {
			case AWAITING_ACCEPT:
				return handleAwaitingAccept(transportPacket);
			case SENDING_MESSAGE:
				return handleSendingMessage(transportPacket);
			case FINISHED:
				return handleFinished(transportPacket);
			default:
				return false;
		}
	}

	private boolean validateMessage(TransportPacket transportPacket) {
		return transportPacket.isValid() && currentTransportPacket != null
				&& transportPacket.getDestinationId() == currentTransportPacket.getSourceId()
				&& transportPacket.getSourceId() == currentTransportPacket.getDestinationId();
	}

	private boolean handleFinished(TransportPacket transportPacket) {
		if(transportPacket.getType().equals(TransportPacket.Type.FINISHED)) {
			sendAcknowledged(transportPacket.getSourceId(), transportPacket.getDestinationId(), transportPacket.getSequenceNumber());
			reset();
			listener.onFinished();
			return true;
		}
		return false;
	}

	private boolean handleSendingMessage(TransportPacket transportPacket) {
		if(transportPacket.getType().equals(TransportPacket.Type.ACKNOWLEDGE)) {
			if(dataPackets.hasNext()) {
				currentTransportPacket = dataPackets.next();
				sendNextPacket();
				return true;
			} else {
				setNextState(State.FINISHED);
				sendFinished(transportPacket.getDestinationId(),
						transportPacket.getSourceId(),
						transportPacket.getSequenceNumber());
				return true;
			}
		}
		return false;
	}

	private boolean handleAwaitingAccept(TransportPacket transportPacket) {
		if (transportPacket.getType().equals(TransportPacket.Type.AWAITING_ORDERS)) {
			setNextState(State.SENDING_MESSAGE);
			sendData(transportPacket.getDestinationId(),
					transportPacket.getSourceId(),
					transportPacket.getSequenceNumber());
			return true;
		}
		return false;
	}

	@Override
	public void onTimeout() {
		if(shouldTimeout()) {
			if (numRetries >= maxRetries) {
				if(timeoutCausesError()) {
					listener.onTimeout(new Exception(String.format("Timed out in state %s", currentState.name())));
				} else {
					listener.onFinished();
				}
				reset();
			} else {
				resendCurrentMessage();
			}
		}
	}

	private boolean timeoutCausesError() {
		return currentState != State.FINISHED;
	}

	private boolean shouldTimeout() {
		return currentState != State.IDLE;
	}

	private void setCurrentState(State currentState) {
		Log.d(Client.class.getSimpleName(), "setCurrentState: " + currentState);
		this.currentState = currentState;
	}

	private void setNextState(State nextState) {
		Log.d(Client.class.getSimpleName(), "setNextState: " + nextState);
		this.nextState = nextState;
	}
}
