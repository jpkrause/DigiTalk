/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.physical;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PhysicalLayerParams {
	private int baud;
	private int sampleRate; // audio samples per second
	private double frequencyZero; //hz
	private double frequencyOne; // hz
	private int numStartBits, numStopBits, numParityBits, numDataBits;
	private int minimumFrequencyPower;

	private PhysicalLayerParams(){}

	public static PhysicalLayerParams customParams(int baud,
							   double frequencyZero,
							   double frequencyOne,
							   int minimumFrequencyPower) {
		PhysicalLayerParams params = defaultParams();
		params.baud = baud;
		params.frequencyZero = frequencyZero;
		params.frequencyOne = frequencyOne;
		params.minimumFrequencyPower = minimumFrequencyPower;
		return params;
	}

	public static PhysicalLayerParams defaultParams() {
		PhysicalLayerParams params = new PhysicalLayerParams();
		params.baud = 63; // 63 max
		params.sampleRate = 44100; // audio samples per second
		params.frequencyZero = 1070; //hz
		params.frequencyOne = 1270; // hz
		params.numStartBits = 1;
		params.numStopBits = 1;
		params.numParityBits = 0;
		params.numDataBits = 8;
		params.minimumFrequencyPower = 10;
		return params;
	}

	public static PhysicalLayerParams experimentalParams() {
		PhysicalLayerParams params = defaultParams();
		params.frequencyZero = 13070; //hz
		params.frequencyOne = 13270; // hz
		params.minimumFrequencyPower = 10;
		return params;
	}

	public static List<PhysicalLayerParams> getAll() {
		ArrayList<PhysicalLayerParams> all = new ArrayList<>();
		all.add(defaultParams());
		all.add(experimentalParams());
		return all;
	}

	public int getBaud() {
		return baud;
	}

	public void setBaud(int baud) {
		this.baud = baud;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	public int getSamplesPerBit() {
		return sampleRate/baud;
	}

	public double getFrequencyZero() {
		return frequencyZero;
	}

	public void setFrequencyZero(double frequencyZero) {
		this.frequencyZero = frequencyZero;
	}

	public double getFrequencyOne() {
		return frequencyOne;
	}

	public void setFrequencyOne(double frequencyOne) {
		this.frequencyOne = frequencyOne;
	}

	public int getNumStartBits() {
		return numStartBits;
	}

	public void setNumStartBits(int numStartBits) {
		this.numStartBits = numStartBits;
	}

	public int getNumStopBits() {
		return numStopBits;
	}

	public void setNumStopBits(int numStopBits) {
		this.numStopBits = numStopBits;
	}

	public int getNumParityBits() {
		return numParityBits;
	}

	public void setNumParityBits(int numParityBits) {
		this.numParityBits = numParityBits;
	}

	public int getNumDataBits() {
		return numDataBits;
	}

	public void setNumDataBits(int numDataBits) {
		this.numDataBits = numDataBits;
	}

	public int getBitsPerFrame() {
		return numStartBits + numDataBits + numParityBits + numStopBits;
	}

	public int getStartBitIndex() {
		return 0;
	}

	public int getStopBitIndex() {
		return getBitsPerFrame() - 1;
	}

	public int getParityBitIndex() {
		return getBitsPerFrame() - numStopBits - 1;
	}

	public int getFirstDataBitIndex() {
		return getStartBitIndex() + 1;
	}

	public int getLastDataBitIndex() {
		return getBitsPerFrame() - numStopBits - numParityBits - 1;
	}

	public int getMinimumFrequencyPower() {
		return minimumFrequencyPower;
	}

	public void setMinimumFrequencyPower(int minimumFrequencyPower) {
		this.minimumFrequencyPower = minimumFrequencyPower;
	}

	public long getCarrierSenseWaitTimeMillis() {
		return getBitsPerFrame()*(TimeUnit.SECONDS.toMillis(2)/baud);
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "%.0f hz - %.0f hz, %d baud", frequencyZero, frequencyOne, baud);
	}
}
