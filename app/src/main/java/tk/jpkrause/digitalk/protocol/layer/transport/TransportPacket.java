/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.transport;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.zip.CRC32;

class TransportPacket {

	public static final int MAX_DATA_BYTES = 32;
	private static final int CRC_SIZE_BYTES = 4;
	private static final int HEADER_SIZE_BYTES = CRC_SIZE_BYTES + 5;

	private CRC32 crc32 = new CRC32();

	enum Type {
		SUMMON,
		ACKNOWLEDGE,
		AWAITING_ORDERS,
		DATA,
		FINISHED;
		public byte getByteValue() {
			return (byte)this.ordinal();
		}
		public static Type fromByteValue(byte value) {
			return Type.values()[value];
		}
	}
	private byte sourceId, destinationId, sequenceNumber, acknowledgmentNumber;
	private int checksum;
	private byte[] data;
	private Type type;

	private TransportPacket(){}

	TransportPacket(byte sourceId,
							  byte destinationId,
							  byte sequenceNumber,
							  byte acknowledgmentNumber,
							  Type type,
							  byte[] data) {
		this.sourceId = sourceId;
		this.destinationId = destinationId;
		this.sequenceNumber = sequenceNumber;
		this.acknowledgmentNumber = acknowledgmentNumber;
		this.type = type;
		this.data = data;
		this.checksum = calculateCRC32();
	}

	private int calculateCRC32() {
		crc32.reset();
		crc32.update(sourceId);
		crc32.update(destinationId);
		crc32.update(sequenceNumber);
		crc32.update(acknowledgmentNumber);
		crc32.update(type.getByteValue());
		if(data != null) {
			crc32.update(data);
		}
		return (int)crc32.getValue();
	}

	public byte[] compile() {
		int dataSize = data == null ? 0 : data.length;
		ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE_BYTES + dataSize);
		buffer.put(sourceId);
		buffer.put(destinationId);
		buffer.put(sequenceNumber);
		buffer.put(acknowledgmentNumber);
		buffer.put(type.getByteValue());
		for(int i = CRC_SIZE_BYTES - 1; i >= 0; i--) {
			buffer.put((byte)((checksum >>> i*8) & 0xFF));
		}
		if(data != null) {
			buffer.put(data);
		}
		return buffer.array();
	}

	public static TransportPacket fromBytes(byte[] data) {
		ByteBuffer byteBuffer = ByteBuffer.wrap(data);
		try {
			TransportPacket packet = new TransportPacket();
			packet.sourceId = byteBuffer.get();
			packet.destinationId = byteBuffer.get();
			packet.sequenceNumber = byteBuffer.get();
			packet.acknowledgmentNumber = byteBuffer.get();
			packet.type = Type.fromByteValue(byteBuffer.get());
			packet.checksum = ((byteBuffer.get() & 0xFF) << 24)
					| ((byteBuffer.get() & 0xFF) << 16)
					| ((byteBuffer.get() & 0xFF) << 8)
					| (byteBuffer.get() & 0xFF);
			if (byteBuffer.hasRemaining()) {
				packet.data = new byte[byteBuffer.remaining()];
				byteBuffer.get(packet.data);
			}
			return packet;
		} catch (BufferUnderflowException e) {
			e.printStackTrace();
			return null;
		}
	}

	public byte getSourceId() {
		return sourceId;
	}

	public byte getDestinationId() {
		return destinationId;
	}

	public byte getSequenceNumber() {
		return sequenceNumber;
	}

	public byte getAcknowledgmentNumber() {
		return acknowledgmentNumber;
	}

	public byte[] getData() {
		return data;
	}

	public Type getType() {
		return type;
	}

	public boolean isValid() {
		return checksum == calculateCRC32();
	}

	@Override
	public String toString() {
		return "TransportPacket{" +
				"type=" + type +
				", data=" + Arrays.toString(data) +
				", checksum=" + checksum +
				", acknowledgmentNumber=" + acknowledgmentNumber +
				", sequenceNumber=" + sequenceNumber +
				", destinationId=" + destinationId +
				", sourceId=" + sourceId +
				'}';
	}
}
