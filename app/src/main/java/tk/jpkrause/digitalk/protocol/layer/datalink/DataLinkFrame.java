/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.datalink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.CRC32;

public class DataLinkFrame {
	private static final byte START_OF_FRAME_DELIM = (byte)0b10101011;
	private static final byte PREAMBLE_BYTE = (byte)0b10101010;
	public static final Byte[] PREAMBLE = {PREAMBLE_BYTE, START_OF_FRAME_DELIM};
	public static final Byte[] INTERPACKET_GAP = new Byte[]{null};
	private static final int MAX_PAYLOAD_SIZE_BYTES = 255;
	private static final int HEADER_SIZE_BYTES = 0;
	private static final int CRC_SIZE_BYTES = 4;
	public static final int MAX_FRAME_SIZE_BYTES = HEADER_SIZE_BYTES
			+ MAX_PAYLOAD_SIZE_BYTES + CRC_SIZE_BYTES;
	private static final CRC32 CRC_32 = new CRC32();
	private byte[] payload;
	private int crc32;

	public static List<DataLinkFrame> createFramesFromBytes(byte[] bytes) {
		if(bytes == null || bytes.length <= 0) {
			return null;
		}
		int numFrames = (int)Math.ceil((double)bytes.length / MAX_PAYLOAD_SIZE_BYTES);
		List<DataLinkFrame> frames = new ArrayList<>(numFrames);
		int bytesProcessed = 0;
		for(int i = 0; i < numFrames; i++) {
			DataLinkFrame dataLinkFrame = new DataLinkFrame();
			int length = Math.min(MAX_PAYLOAD_SIZE_BYTES, bytes.length-bytesProcessed);
			dataLinkFrame.payload = new byte[length];
			System.arraycopy(bytes, bytesProcessed, dataLinkFrame.payload, 0, length);
			CRC_32.reset();
			CRC_32.update(dataLinkFrame.payload);
			dataLinkFrame.crc32 = (int)CRC_32.getValue();
			frames.add(dataLinkFrame);
			bytesProcessed += length;
		}
		return frames;
	}

	public static DataLinkFrame unwrapAndDeserialize(byte[] data) {
		int start = -1;
		for(int i = 0; i < data.length; i++) {
			if(isStartFrameDelim(data[i])) {
				start = i+1;
				break;
			}
		}
		if(start == -1) {
			return null;
		}
		DataLinkFrame dataLinkFrame = new DataLinkFrame();
		dataLinkFrame.payload = new byte[data.length - start - CRC_SIZE_BYTES - HEADER_SIZE_BYTES];
		System.arraycopy(data, start + HEADER_SIZE_BYTES, dataLinkFrame.payload, 0, dataLinkFrame.payload.length);
		int lastIndex = data.length-1;
		dataLinkFrame.crc32 =
				(data[lastIndex]&0xFF)
				| ((data[lastIndex-1]&0xFF) << 8)
				| ((data[lastIndex-2]&0xFF) << 16)
				| ((data[lastIndex-3]&0xFF) << 24);
		return dataLinkFrame;
	}

	public Byte[] wrapAndSerialize() {
		List<Byte> byteBuffer = new ArrayList<>(PREAMBLE.length + HEADER_SIZE_BYTES
				+ payload.length + CRC_SIZE_BYTES + INTERPACKET_GAP.length);
		Collections.addAll(byteBuffer, PREAMBLE);
		for(byte b : payload) {
			byteBuffer.add(b);
		}
		for(int i = CRC_SIZE_BYTES - 1; i >= 0; i--) {
			byteBuffer.add((byte)((crc32 >>> i*8) & 0xFF));
		}
		Collections.addAll(byteBuffer, INTERPACKET_GAP);
		return byteBuffer.toArray(new Byte[byteBuffer.size()]);
	}

	public boolean isValid() {
		CRC_32.reset();
		CRC_32.update(payload);
		return (int)CRC_32.getValue() == crc32;
	}

	public byte[] getPayload() {
		return payload;
	}

	public static boolean isStartFrameDelim(byte data) {
		return data == START_OF_FRAME_DELIM;
	}
}
