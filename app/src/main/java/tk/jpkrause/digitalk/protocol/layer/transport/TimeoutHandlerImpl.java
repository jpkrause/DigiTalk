/*
 * Copyright (c) 2016 John Krause.
 *
 * This file is part of DigiTalk.
 *
 *     DigiTalk is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     DigiTalk is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tk.jpkrause.digitalk.protocol.layer.transport;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TimeoutHandlerImpl implements TimeoutHandler {
	private Callback callback;
	private ScheduledExecutorService timer;
	private ScheduledFuture timeoutFuture;
	private Runnable timeoutRunnable = new Runnable() {
		@Override
		public void run() {
			callback.onTimeout();
		}
	};
	private boolean isPaused;
	private long remainingTime;

	public TimeoutHandlerImpl() {
		timer = Executors.newScheduledThreadPool(1);
	}

	@Override
	public void start(long timeoutMillis, Callback callback) {
		isPaused = false;
		stop();
		this.callback = callback;
		start(timeoutMillis);
	}

	@Override
	public void stop() {
		isPaused = false;
		stopTimeout();
	}

	private synchronized void start(long time) {
		timeoutFuture = timer.schedule(timeoutRunnable, time, TimeUnit.MILLISECONDS);
	}

	private synchronized void stopTimeout() {
		if(timeoutFuture != null) {
			timeoutFuture.cancel(true);
			timeoutFuture = null;
		}
	}

	@Override
	public void pause() {
		if(timeoutFuture != null) {
			remainingTime = timeoutFuture.getDelay(TimeUnit.MILLISECONDS);
			isPaused = true;
			stopTimeout();
		}
	}

	@Override
	public void resume() {
		if(isPaused) {
			isPaused = false;
			start(remainingTime);
		}
	}
}
